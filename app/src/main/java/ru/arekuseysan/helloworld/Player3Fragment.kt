package ru.arekuseysan.helloworld

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton

class Player3Fragment : Fragment() {
	private var paperRadioButton: RadioButton? = null
	private var scissorsRadioButton: RadioButton? = null
	private var stoneRadioButton: RadioButton? = null

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater!!.inflate(R.layout.fragment_player3, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		paperRadioButton = getView()!!.findViewById(R.id.Player3PaperRadioButton)
		scissorsRadioButton = getView()!!.findViewById(R.id.Player3ScissorsRadioButton)
		stoneRadioButton = getView()!!.findViewById(R.id.Player3StoneRadioButton)

		paperRadioButton!!.setOnClickListener { MainActivity.thirdPlayerChoice = Move.PAPER }
		scissorsRadioButton!!.setOnClickListener { MainActivity.thirdPlayerChoice = Move.SCISSORS }
		stoneRadioButton!!.setOnClickListener { MainActivity.thirdPlayerChoice = Move.ROCK }
	}
}

package ru.arekuseysan.helloworld

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton

class Player2Fragment : Fragment() {
	private var paperRadioButton: RadioButton? = null
	private var scissorsRadioButton: RadioButton? = null
	private var stoneRadioButton: RadioButton? = null

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater!!.inflate(R.layout.fragment_player2, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		paperRadioButton = getView()!!.findViewById(R.id.Player2PaperRadioButton)
		scissorsRadioButton = getView()!!.findViewById(R.id.Player2ScissorsRadioButton)
		stoneRadioButton = getView()!!.findViewById(R.id.Player2StoneRadioButton)

		paperRadioButton!!.setOnClickListener { MainActivity.secondPlayerChoice = Move.PAPER }
		scissorsRadioButton!!.setOnClickListener { MainActivity.secondPlayerChoice = Move.SCISSORS }
		stoneRadioButton!!.setOnClickListener { MainActivity.secondPlayerChoice = Move.ROCK }
	}
}

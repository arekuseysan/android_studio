package ru.arekuseysan.helloworld


enum class Move(private val value: String) {
	ROCK("Камень"),
	PAPER("Бумага"),
	SCISSORS("Ножницы")
}

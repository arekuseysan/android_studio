package ru.arekuseysan.helloworld

import android.app.Fragment
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
	private var resultButton: Button? = null
	private var resultTextView: TextView? = null
	private var seekBar: SeekBar? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		resultButton = findViewById<View>(R.id.resultButton) as Button
		seekBar = findViewById<View>(R.id.seekBar) as SeekBar
		resultTextView = findViewById<View>(R.id.resultTextView) as TextView

		seekBar!!.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
			override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
				if (progress < 33) {
					loadFragment(Player1Fragment())
				} else if (progress in 33..76) {
					loadFragment(Player2Fragment())
				} else {
					loadFragment(Player3Fragment())
				}
			}

			override fun onStartTrackingTouch(seekBar: SeekBar) {}
			override fun onStopTrackingTouch(seekBar: SeekBar) {}
		})

		resultButton!!.setOnClickListener(View.OnClickListener {
			if (firstPlayerChoice == null || secondPlayerChoice == null || thirdPlayerChoice == null) {
				resultTextView!!.text = "Не выбран ход"
				return@OnClickListener
			} else {
				val win1: Int = resultWin(firstPlayerChoice!!, secondPlayerChoice!!, thirdPlayerChoice!!)

				when (win1) {
					0 -> {
						resultTextView!!.text = "Ничья"
					}

					1 -> {
						resultTextView!!.text = "Победил 1"
					}

					2 -> {
						resultTextView!!.text = "Победил 2"
					}

					3 -> {
						resultTextView!!.text = "Победил 3"
					}
				}
			}
		})
	}

	private fun resultWin(first: Move, second: Move, third: Move): Int {
		return when (first) {
			Move.ROCK -> when (second) {
				Move.ROCK -> when (third) {
					Move.PAPER -> 3
					else -> 0
				}

				Move.PAPER -> when (third) {
					Move.ROCK -> 2
					else -> 0
				}

				Move.SCISSORS -> when (third) {
					Move.SCISSORS -> 1
					else -> 0
				}
			}

			Move.PAPER -> when (second) {
				Move.ROCK -> when (third) {
					Move.ROCK -> 1
					else -> 0
				}

				Move.PAPER -> when (third) {
					Move.SCISSORS -> 3
					else -> 0
				}

				Move.SCISSORS -> when (third) {
					Move.PAPER -> 2
					else -> 0
				}
			}

			Move.SCISSORS -> when (second) {
				Move.ROCK -> when (third) {
					Move.SCISSORS -> 2
					else -> 0
				}

				Move.PAPER -> when (third) {
					Move.PAPER -> 1
					else -> 0
				}

				Move.SCISSORS -> when (third) {
					Move.ROCK -> 3
					else -> 0
				}
			}
		}
	}

	private fun loadFragment(fragment: Fragment) {
		val fm = fragmentManager
		val fragmentTransaction = fm.beginTransaction()
		fragmentTransaction.replace(R.id.frameLayout, fragment)
		fragmentTransaction.commit()
	}

	companion object {
		var firstPlayerChoice: Move? = null
		var secondPlayerChoice: Move? = null
		var thirdPlayerChoice: Move? = null
	}
}
